![eduinf-logo](eduinf.png)

# ESP-32 Toolbox 

## Funktionsumfang (25.04.2021)
- Übersetzung zwischen internen & externen Pinnummern
- Trennung Eingabe-/Ausgabepins
- Vereinfachte Pindefinition über `connectInput(pin)` und `connectOutput(pin)`
- Ansteuerung integrierter LED über `onboardLEDon()` und `onboardLEDoff()` - Pins standardmäßig auf GND
- Verbindung von Servos als PWM-Objekt über `connectServo(pin)`
- Übersetzung des Servo-PWM-Tastgrads in Winkel; Ansteuerung über `setAngle(angle)`
- Zusammenführung aller Funktionalitäten in Modul `toolbox.py` 

## Pin-Translation

| Input-Pin | ESP32-GPIO | ADC1-Channel |   
|:---------:|:----------:|:------------:|  
| 1         | 32         | 04           |
| 2         | 33         | 05           |
| 3         | 34         | 06           |
| 4         | 35         | 07           |
| 5         | 36 (SP/VP) | 0            |

| Output-Pin| ESP32-GPIO |
|:---------:|:----------:|
| 1         | 23         |
| 2         | 19         |
| 3         | 18         |
| 4         | 17 (TX2)   |
| 5         | 16 (RX2)   |

