# This file is executed on every boot (including wake-boot from deepsleep)
#import esp
#esp.osdebug(None)
#import webrepl
#webrepl.start()


from machine import Pin, PWM, ADC, time_pulse_us
from time import sleep, sleep_ms
import neopixel


"""
Setup sensors and actors
"""
class US:
    """
    https://how2electronics.com/esp32-micropython-interfacing-ultrasonic-sensor-hc-sr04/
    __version__ = '0.2.0'
    __author__ = 'Roberto Sánchez'
    __license__ = "Apache License 2.0. https://www.apache.org/licenses/LICENSE-2.0"
    Driver to use the untrasonic sensor HC-SR04.
    The sensor range is between 2cm and 4m.
    The timeouts received listening to echo pin are converted to OSError('Out of range')
    """
    # echo_timeout_us is based in chip range limit (400cm)
    def __init__(self, trigger_pin, echo_pin, echo_timeout_us=500*2*30):
        """
        trigger_pin: Output pin to send pulses
        echo_pin: Readonly pin to measure the distance. The pin should be protected with 1k resistor
        echo_timeout_us: Timeout in microseconds to listen to echo pin. 
        By default is based in sensor limit range (4m)
        """
        self.echo_timeout_us = echo_timeout_us
        # Init trigger pin (out)
        self.trigger = Pin(trigger_pin, mode=Pin.OUT, pull=None)
        self.trigger.value(0)
 
        # Init echo pin (in)
        self.echo = Pin(echo_pin, mode=Pin.IN, pull=None)
 
    def _send_pulse_and_wait(self):
        """
        Send the pulse to trigger and listen on echo pin.
        We use the method `machine.time_pulse_us()` to get the microseconds until the echo is received.
        """
        self.trigger.value(0) # Stabilize the sensor
        sleep_us(5)
        self.trigger.value(1)
        # Send a 10us pulse.
        sleep_us(10)
        self.trigger.value(0)
        try:
            pulse_time = time_pulse_us(self.echo, 1, self.echo_timeout_us)
            return pulse_time
        except OSError as ex:
            if ex.args[0] == 110: # 110 = ETIMEDOUT
                raise OSError('Out of range')
            raise ex
 
    def distance_mm(self):
        """
        Get the distance in milimeters without floating point operations.
        """
        pulse_time = self._send_pulse_and_wait()
 
        # To calculate the distance we get the pulse_time and divide it by 2 
        # (the pulse walk the distance twice) and by 29.1 becasue
        # the sound speed on air (343.2 m/s), that It's equivalent to
        # 0.34320 mm/us that is 1mm each 2.91us
        # pulse_time // 2 // 2.91 -> pulse_time // 5.82 -> pulse_time * 100 // 582 
        mm = pulse_time * 100 // 582
        return mm
 
    def distance_cm(self):
        """
        Get the distance in centimeters with floating point operations.
        It returns a float
        """
        pulse_time = self._send_pulse_and_wait()
 
        # To calculate the distance we get the pulse_time and divide it by 2 
        # (the pulse walk the distance twice) and by 29.1 becasue
        # the sound speed on air (343.2 m/s), that It's equivalent to
        # 0.034320 cm/us that is 1cm each 29.1us
        cms = (pulse_time / 2) / 29.1
        return cms


"""
Setup
Pin configuration 
"""

'''
old config
inputPins = {
    1: 16,
    2: 17,
    3: 18,
    4: 19,
    5: 21
}
'''

#pin translation
inputPins = {
    1: 32,
    2: 33,
    3: 34,
    4: 35,
    5: 36
}

'''
old config
outputPins = {
    1: 27,
    2: 26,
    3: 25,
    4: 23,
    5: 22
}
'''

outputPins = {
    1: 23,
    2: 19,
    3: 18,
    4: 17,
    5: 16
}



"""
Main functions
"""


def connectLED():
    global onboardLED
    onboardLED = Pin(13, Pin.OUT)

def onboardLEDon():
    if not "onboardLED" in globals():
        connectLED()
    onboardLED.value(1)

def onboardLEDoff():
    if "onboardLED" in globals():
        onboardLED.value(0)

def connectInput(pin):
    return Pin(inputPins[pin], Pin.IN, Pin.PULL_DOWN)

#FIX analogInputs
def connectAnalogInput(pin):
    return ADC(Pin(inputPins[pin], Pin.IN, Pin.PULL_DOWN))


def connectOutput(pin):
    return Pin(outputPins[pin], Pin.OUT, Pin.PULL_DOWN)



# Servo

# Angle to PWM-Translation
# 0°   PWM = 30
# 90°  PWM = 77
# 180° PWM = 124
# dPWM 0-180° = 94

def connectServo(pin):
    global servo
    servo = PWM(connectOutput(pin), freq=50)


def setAngle(angle):
    dutycycle = int(30 + int(angle)/180 * 94)
    servo.duty(dutycycle)


# Ultrasonic
def connectUS(trigger_pin, echo_pin, timeout):
    global ussensor
    ussensor = sensor = HCSR04(inputPins[trigger_pin], inputPins[echo_pin],timeout=1000000)

# Neopixel
def connectNeopixel(pin, number_of_pixels):
    global np
    np = neopixel.NeoPixel(Pin(outputPins[pin]), number_of_pixels)

